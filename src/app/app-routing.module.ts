import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// IMPORT MAIN LAYOUTS ("MASTER PAGES")
import { PublicComponent } from './layouts/public/public.component';
import { ProtectedComponent } from './layouts/protected/protected.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
// IMPORT PUBLIC "PAGES"
import { LoginComponent } from './pages/public/login/login.component';
// IMPORT PROTECTED PAGES
import { DashboardComponent } from './pages/protected/dashboard/dashboard.component';
import { SearchComponent } from './pages/protected/search/search.component';
import { OrderDetailsComponent } from './pages/protected/order-details/order-details.component';
import { AuthGuard } from './guards/auth.guard';
import { GuestGuard } from './guards/guest.guard';

const routes: Routes = [
  // PUBLIC ROUTES
  {
    path: 'login',
    component: PublicComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [GuestGuard],
    children: [
      { path: '', component: LoginComponent, pathMatch: 'full' }
    ]
   },

   // PROTECTED ROUTES
   {
    path: '',
    component: ProtectedComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent, pathMatch: 'full' },
      { path: 'search', component: SearchComponent },
      { path: 'order/:orderid', component: OrderDetailsComponent },
    ]
  },

  // 404 NOT FOUND
  {
    path: '**',
    component: NotFoundComponent
  }

  // OTHERWISE REDIRECT TO HOME
  // { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
