import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Search } from '../models/search.model';

const searchUrl = environment.apiBase + environment.apiCollections.search;

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  search(searchModel: Search): Observable<any> {
    return this.http.post(searchUrl, searchModel);
  }
}
