import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AlertifyService } from './alertify.service';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const authLoginUrl = environment.apiBase + environment.apiCollections.authenticate.signin;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwt = new JwtHelperService();
  decodedToken: any;

  constructor(private http: HttpClient, private alertify: AlertifyService, private router: Router) { }

  login(model): Observable<any> {
    return this.http.post(authLoginUrl, model).pipe(
      map(
        (response: any) => {
          const user = response;
          if (user && user.status === true) {
            localStorage.setItem('token', response.LoginResults);
            this.decodedToken = this.jwt.decodeToken(response.LoginResults);
          }
          return response;
        }
      )
    );
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwt.isTokenExpired(token);
  }

  logout() {
    localStorage.removeItem('token');
    sessionStorage.removeItem('activeMerchant');
    this.router.navigate(['/login']);
    this.alertify.message('You have successfully logged out.');
  }
}
