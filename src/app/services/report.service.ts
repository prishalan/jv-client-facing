import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor() { }

  drawReport(reportId: string, merchantFilter: string, elementToAttach: string) {
    let reportOptions = {
      reportUUID        : reportId,
      username          : environment.yellowfin.credentials.username,
      password          : environment.yellowfin.credentials.password,
      elementId         : document.getElementById(elementToAttach).id,
      showFilters       : false,
      showSections      : false,
      showExport        : false,
      showSeries        : false,
      showPageLinks     : false,
      showInfo          : false,
      showTitle         : false,
      fitTableWidth	    : true
    };

    
  }
}
