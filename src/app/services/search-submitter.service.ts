import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export const enum SearchParameterType {
  Customer = 1,
  Order = 2
}

@Injectable({
  providedIn: 'root'
})
export class SearchSubmitterService {

  private inputParams = new BehaviorSubject<any>(null);
  currentParams = this.inputParams.asObservable();

  constructor() { }

  sendParams(params: any, type: SearchParameterType) {
    this.inputParams.next({ params: params, type: type });
  }
}
