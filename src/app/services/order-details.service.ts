import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const token = localStorage.getItem('token');
const detailsUrl = environment.apiBase + environment.apiCollections.order.details;
const additionalUrl = environment.apiBase + environment.apiCollections.order.additional;
const courierUrl = environment.apiBase + environment.apiCollections.order.courier;
const progressUrl = environment.apiBase + environment.apiCollections.order.progress;
const commentsUrl = environment.apiBase + environment.apiCollections.order.comments;
const documentsUrl = environment.apiBase + environment.apiCollections.order.documents;
const dispatchUrl = environment.apiBase + environment.apiCollections.order.dispatch;

@Injectable({
  providedIn: 'root'
})
export class OrderDetailsService {

  constructor(private http: HttpClient) { }

  getOrderDetails(orderId: string): Observable<any> {
    return this.http.post(detailsUrl, { OrderNumber: orderId, token: token });
  }

  getOrderAdditionalDetails(transactionId: number): Observable<any> {
    return this.http.post(additionalUrl, { TransactionId: transactionId, token: token });
  }

  getOrderCourierDetails(transactionId: number): Observable<any> {
    return this.http.post(courierUrl, { TransactionId: transactionId, token: token });
  }

  getOrderProgressDetails(transactionId: number): Observable<any> {
    return this.http.post(progressUrl, { transaction_id: transactionId, token: token });
  }

  getOrderComments(transactionId: number): Observable<any> {
    return this.http.post(commentsUrl, { TransactionId: transactionId, token: token });
  }

  getOrderDocuments(transactionId: number): Observable<any> {
    return this.http.post(documentsUrl, { TransactionId: transactionId, token: token });
  }

  getOrderDispatch(transactionId: number): Observable<any> {
    return this.http.post(dispatchUrl, { TransactionId: transactionId, token: token });
  }
}
