import { Component, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { TitleService } from '../../../services/title.service';
import { ScriptLoaderService } from '../../../services/script-loader.service';
import { environment } from 'src/environments/environment.prod';
import { AlertifyService } from 'src/app/services/alertify.service';

declare let yellowfin: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  activeMerchant: any = {};

  constructor(
    private titleService: TitleService,
    private scriptLoaderService: ScriptLoaderService,
    private alertify: AlertifyService
  ) { }

  ngOnInit() {
    this.activeMerchant = JSON.parse(sessionStorage.getItem('activeMerchant'));
    this.titleService.emit('Dashboard (' + this.activeMerchant.StoreName + ')');
    // console.dir(this.activeMerchant.StoreName);
    this.initScripts();
  }

  private initScripts() {
    // You can load multiple scripts by just providing the key as argument into load method of the service
    this.scriptLoaderService.load('require','jquery','yellowfin', 'yellowfin-reports').then(data => {
      if(data[0].loaded) {
        this.loadReports();
      }
    }).catch(error => console.log(error));
  }

  switchMerchantTitle(title) {
    this.titleService.emit('Dashboard (' + title + ')');
  }

  syncActiveMerchant() {
    this.activeMerchant = JSON.parse(sessionStorage.getItem('activeMerchant'));
    console.dir(this.activeMerchant.StoreName);
  }

  loadReports() {
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.trend.performanceTrend, 'dash-trends-performance', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.contactCentre.addressVerificationRate, 'dash-contactcentre-avr', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.contactCentre.outbound, 'dash-contactcentre-outbound', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.contactCentre.inbound, 'dash-contactcentre-inbound', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.logistics.deliveryRate, 'dash-logistics-deliveryrate', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.logistics.courierPerformance, 'dash-logistics-courierperformance', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.nonDelivery.cancellationRate, 'dash-nd-cancellationrate', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.nonDelivery.returnRate, 'dash-nd-returnrate', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.nonDelivery.cancellationReason, 'dash-nd-cancellationreason', true);
    this.getReport(this.activeMerchant.StoreName, environment.yellowfin.reports.nonDelivery.returnReason, 'dash-nd-returnreason', true);
  }

  getReport(merchantName: string, reportUUID: string, elementToAttach: string, elementResult: any) {
    yellowfin.reports.loadReportFilters(reportUUID, function (filters) {
      // console.dir(filters);
      let filterIndex = -1;

      filters.forEach((filter, i) => {
				if (filter.description === "Merchant") {
					filterIndex = i;
					return;
				}
			});

      if (filterIndex >= 0) {
				let merchants = filters[filterIndex].listValues;
				let found = false;
        let filterValues = {};

        merchants.forEach((merchant) => {
          // console.dir(this.activeMerchant);
					// if (merchant.value === this.activeMerchant.StoreName) {
					if (merchant.value === merchantName) {
						found = true;
						return;
					}
				});

        if (found) {
          // filterValues[filters[filterIndex].filterUUID] = this.activeMerchant.StoreName;
          filterValues[filters[filterIndex].filterUUID] = merchantName;
          let options = {
            reportUUID        : reportUUID,
            username          : environment.yellowfin.credentials.username,
            password          : environment.yellowfin.credentials.password,
            elementId         : document.getElementById(elementToAttach).id,
            showFilters       : false,
            showSections      : false,
            showExport        : false,
            showSeries        : false,
            showPageLinks     : false,
            showInfo          : false,
            showTitle         : false,
            showFooter	      : false,
            fitTableWidth	    : true,
						filters           :	filterValues
          };
					yellowfin.loadReport(options);
        } else {
          // don't render container for graph
        }
      } else {
        // TODO: show report name
        // this.alertify.error('There was an error processing this report...')
        console.log('There was an error processing this report...');
      }
    });
  }

}
