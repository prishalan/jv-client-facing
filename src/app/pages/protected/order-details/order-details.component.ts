import { Component, OnInit } from '@angular/core';
import { OrderDetailsService } from 'src/app/services/order-details.service';
import { ActivatedRoute, Params } from '@angular/router';
import { TitleService } from 'src/app/services/title.service';
import { environment } from 'src/environments/environment';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation, NgxGalleryImageSize } from 'ngx-gallery';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  data: any = {};
  orderId: string;
  transactionId: number = null;
  dataGeneralDetails: any = {};
  dataDispatchDetails: any = [];
  dataOrderProducts: any = [];
  dataAdditionalInfo: any = {};
  dataCourierDetails: any = [];
  dataOrderComments: any = [];
  dataOrderProgressDetails: any = [];
  // dataOrderDocuments: any = [];
  showNoData = true;
  loadingData = true;
  documentsBaseUrl = environment.documentsUrlBase;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: Array<NgxGalleryImage> = [];
  productsTotal: number = 0.00;

  constructor(
    private orderDetailsService: OrderDetailsService,
    private route: ActivatedRoute,
    private titleService: TitleService
  ) {
    this.galleryOptions = [
      {
        width: '100%',
        image: false,
        height: '100px',
        imageSize: NgxGalleryImageSize.Contain,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageDescription: false,
        previewKeyboardNavigation: true,
        previewFullscreen: true,
        previewSwipe: true,
        previewCloseOnClick: true,
        previewCloseOnEsc: true,
        previewZoom: true,
        thumbnailsRemainingCount: true
      },
      // max-width 800
      {
          breakpoint: 800,
          width: '100%',
          height: '100px',
          imagePercent: 90,
          thumbnailsPercent: 20,
          thumbnailsMargin: 20,
          thumbnailMargin: 20,
      },
      // max-width 400
      {
          breakpoint: 400,
          preview: true
      }
    ];
  }

  ngOnInit() {
    this.data.loadingData = true;
    this.route.params.subscribe(
      (params: Params) => {
        this.orderId = params['orderid']
      }
    );
    this.titleService.emit('Order Details: ' + this.orderId);

    this.orderDetailsService.getOrderDetails(this.orderId).subscribe(
      (data: any) => {
        if (data) {
          if (data.Exception) {
            this.showNoData = true;
            this.loadingData = false;
          } else {
            this.showNoData = false;
            // console.dir(data);
            this.dataGeneralDetails = data['General Details'];
            this.dataOrderProducts = data['Order Products'];
            this.transactionId = this.dataGeneralDetails.Transaction_Id;

            if (this.transactionId) {
              this.getAdditionalInfo();
              this.getOrderDispatchDetails();
              this.getCourierDetails();
              this.getOrderComments();
              this.getOrderProgressDetails();
              this.getOrderDocuments();
            }
            this.loadingData = false;
          }
        } else {
          this.showNoData = true;
          this.loadingData = false;
        }
      }
    );
  }

  getOrderDispatchDetails() {
    this.orderDetailsService.getOrderDispatch(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data);
        this.dataDispatchDetails = data.Dispatch;
      }
    )
  }

  getAdditionalInfo() {
    this.orderDetailsService.getOrderAdditionalDetails(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data);
        this.dataAdditionalInfo = data['Additional Details'][0];
      }
    )
  }

  getCourierDetails() {
    this.orderDetailsService.getOrderCourierDetails(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data);
        this.dataCourierDetails = data.Courier;
      }
    )
  }

  getOrderComments() {
    this.orderDetailsService.getOrderComments(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data);
        this.dataOrderComments = data.Comments;
      }
    )
  }

  getOrderProgressDetails() {
    this.orderDetailsService.getOrderProgressDetails(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data);
        this.dataOrderProgressDetails = data.Progress;
      }
    )
  }

  getOrderDocuments() {
    this.orderDetailsService.getOrderDocuments(this.transactionId).subscribe(
      (data: any) => {
        // console.dir(data.Documents.reverse());
        // this.dataOrderDocuments = data.Documents;
        for (let image of data.Documents.reverse()) {
          this.galleryImages.push({
            "small": this.documentsBaseUrl + image.ImageName,
            "big": this.documentsBaseUrl + image.ImageName,
            "description": 'Document ' + image.Sequence + ' of ' + data.Documents.length
          });
        };
      }
    )
  }

  switchTab(event, ref: string) {
    [].forEach.call(document.querySelectorAll('.tab-header > li > a'), (header) => {
      header.classList.remove('active');
    });
    [].forEach.call(document.querySelectorAll('.tab'), (tab) => {
      tab.classList.remove('active');
    });
    document.getElementById(ref).classList.add('active');
    event.target.classList.add('active');
  }

  getProductsBilledTotal() {
    let sum = 0.00;
    for (let i = 0; i < this.dataOrderProducts.length; i++) {
      sum += this.dataOrderProducts[i].BilledPrice;
    }
    return sum;
  }

}
