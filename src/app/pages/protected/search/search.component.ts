import { Component, OnInit, OnDestroy } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';
import { Subscription } from 'rxjs';
import { SearchSubmitterService, SearchParameterType } from 'src/app/services/search-submitter.service';
import { SearchService } from 'src/app/services/search.service';
import { Search } from 'src/app/models/search.model';
import { AlertifyService } from 'src/app/services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  searchParams: any;
  subscription: Subscription;
  searchResultsMessage: string = null;
  searchResults: any = [];
  showSearchResultsTable = false;

  constructor(
    private titleService: TitleService,
    private searchSubmitterService: SearchSubmitterService,
    private searchService: SearchService,
    private alertify: AlertifyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.titleService.emit('Search Results');
    this.subscription = this.searchSubmitterService.currentParams.subscribe(
      (params) => {
        this.searchParams = params;
        if (this.searchParams) {
          const token = localStorage.getItem('token');
          let searchModel = new Search();
          switch (this.searchParams.type) {
            case (SearchParameterType.Customer):
              searchModel.CustomerIdNumber = (this.searchParams.params.customerId) ? this.searchParams.params.customerId : null;
              searchModel.CustomerName = (this.searchParams.params.customerName) ? this.searchParams.params.customerName : null;
              searchModel.OrderNumber = null;
              searchModel.ExtReference = null;
              searchModel.token = token;
              // console.dir(searchModel);
              break;
            case (SearchParameterType.Order):
              searchModel.CustomerIdNumber = null;
              searchModel.CustomerName = null;
              searchModel.OrderNumber = (this.searchParams.params.orderNumber) ? this.searchParams.params.orderNumber : null;
              searchModel.ExtReference = (this.searchParams.params.orderExternalReference) ? this.searchParams.params.orderExternalReference : null;
              searchModel.token = token;
              // console.dir(searchModel);
              break;
            default:
          }
          this.searchService.search(searchModel).subscribe(
            (data: any) => {
              if (data) {
                this.searchResultsMessage = 'Your search yielded <strong>' + data.length + ' </strong>results.';
                this.searchResults = data;
                this.showSearchResultsTable = true;
                // console.dir(this.searchResults);
              } else {
                this.showSearchResultsTable = false;
                this.searchResultsMessage = 'No results were found for your search criteria';
              }
            }
          )
        }
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  viewOrderDetails(orderId: string) {
    this.router.navigate(['/order', orderId]);
  }

}
