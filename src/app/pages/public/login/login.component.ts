import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'src/app/services/alertify.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = false;
  showLoginError = false;
  loginErrorMessage = '';
  model: any = {};

  constructor(private auth: AuthService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {}

  login() {
    this.loading = true;
    this.showLoginError = false;

    this.auth.login(this.model).subscribe(
      (data: any) => {
        if (data) {
          if (data.status === false) {
            this.showLoginError = true;
            this.loginErrorMessage = data.LoginResults;
          }
          if (data.status === true) {
            this.alertify.success('You have successfully logged in.');
            this.router.navigate(['']);
          }
        } else {
          this.alertify.error('No response from the server. Please contact an administrator.')
        }
      },
      (error: any) => {
        this.loading = false;
        this.alertify.error('An error occurred: ' + error);
        this.alertify.error('Please try again.');
      },
      () => {
        this.loading = false;
      }
    );
  }
}
