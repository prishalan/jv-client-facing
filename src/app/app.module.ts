import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PublicComponent } from './layouts/public/public.component';
import { ProtectedComponent } from './layouts/protected/protected.component';
import { LoginComponent } from './pages/public/login/login.component';
import { DashboardComponent } from './pages/protected/dashboard/dashboard.component';
import { AlertifyService } from './services/alertify.service';
import { AuthService } from './services/auth.service';
import { ReportService } from './services/report.service';
import { ScriptLoaderService } from './services/script-loader.service';
import { TitleService } from './services/title.service';
import { HttpClientModule } from '@angular/common/http';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { SearchComponent } from './pages/protected/search/search.component';
import { OrderDetailsComponent } from './pages/protected/order-details/order-details.component';
import { AuthGuard } from './guards/auth.guard';
import { GuestGuard } from './guards/guest.guard';
import { SearchSubmitterService } from './services/search-submitter.service';
import { OrderDetailsService } from './services/order-details.service';
import { MenuComponent } from './partials/menu/menu.component';
import { UserComponent } from './partials/user/user.component';
import { MerchantOverlayComponent } from './partials/merchant-overlay/merchant-overlay.component';
import { SearchOverlayComponent } from './partials/search-overlay/search-overlay.component';
import { NgxGalleryModule } from 'ngx-gallery';

@NgModule({
  declarations: [
    AppComponent,
    PublicComponent,
    ProtectedComponent,
    LoginComponent,
    DashboardComponent,
    NotFoundComponent,
    SearchComponent,
    OrderDetailsComponent,
    MenuComponent,
    UserComponent,
    MerchantOverlayComponent,
    SearchOverlayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxGalleryModule
  ],
  providers: [
    AlertifyService,
    AuthService,
    ReportService,
    ScriptLoaderService,
    SearchSubmitterService,
    OrderDetailsService,
    TitleService,
    AuthGuard,
    GuestGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
