import { Component, OnInit, HostListener, ViewChild, ContentChild } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { TitleService } from '../../services/title.service';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from '../../../environments/environment';
import { AlertifyService } from 'src/app/services/alertify.service';
import { DashboardComponent } from 'src/app/pages/protected/dashboard/dashboard.component';
import { SearchSubmitterService, SearchParameterType } from 'src/app/services/search-submitter.service';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.scss'],
  providers: [DashboardComponent]
})
export class ProtectedComponent implements OnInit {
  routeTitle = 'Jarvis';
  showSearch = false;
  showSwitchMerchant = false;
  sidebarMobileActive = false;
  merchantLogoBaseUrl = environment.logoUrlBase;
  merchants: any = [];
  activeMerchant: any = {};
  searchParams: any = {};
  @ContentChild(DashboardComponent) private dashboard: DashboardComponent;

  constructor(
    private router: Router,
    private titleService: TitleService,
    public auth: AuthService,
    private alertify: AlertifyService,
    private searchSubmitterService: SearchSubmitterService
  ) {
    this.titleService.emitChange.subscribe(
      title => {
        this.routeTitle = title;
      }
    );
  }

  ngOnInit() {
    this.merchants = this.auth.decodedToken.Merchants;
    // Check if session object exists for a 'active merchant' and use it, else set session object to the first object in the return array
    let tempMerchant = sessionStorage.getItem('activeMerchant');
    if (tempMerchant) {
      this.activeMerchant = JSON.parse(tempMerchant);
      tempMerchant = null;
    } else {
      sessionStorage.setItem('activeMerchant', JSON.stringify(this.merchants[0]));
      this.activeMerchant = this.merchants[0];
    }
  }

  logout() {
    this.auth.logout();
  }

  toggleSearch() {
    this.showSearch = !this.showSearch;
  }

  toggleSwitchMerchant() {
    this.showSwitchMerchant = !this.showSwitchMerchant;
  }

  toggleMobileSidebar() {
    this.sidebarMobileActive = !this.sidebarMobileActive;
  }

  isActiveMerchant(id) {
    return (id === this.activeMerchant.storeId);
  }

  switchMerchant(index) {
    this.activeMerchant = this.merchants[index];
    sessionStorage.setItem('activeMerchant', JSON.stringify(this.activeMerchant));
    this.showSwitchMerchant = false;
    this.alertify.success('Switched merchant to "' + this.merchants[index].StoreName + '"');
    if (this.router.url === '/') {
      this.dashboard.switchMerchantTitle(this.merchants[index].StoreName);
      // reload reports
      this.dashboard.syncActiveMerchant();
      this.dashboard.loadReports();
    }
    this.hideSidebarOnMobile();
  }

  searchOrderReferenceFormValid() {
    // if (this.searchOrderReferenceForm.get('search_order_number').valid ||
    // this.searchOrderReferenceForm.get('search_external_reference').valid) {
    //   return true;
    // }
    // return false;
  }

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (this.showSearch || this.showSwitchMerchant) {
      this.showSearch = false;
      this.showSwitchMerchant = false;
    }
  }

  submitSearchByOrder(searchType: SearchParameterType) {
    this.router.navigate(['/search']);
    this.searchSubmitterService.sendParams(this.searchParams, searchType);
    this.showSearch = false;
    this.hideSidebarOnMobile();
  }

  hideSidebarOnMobile() {
    if (this.sidebarMobileActive === true) {
      this.sidebarMobileActive = false;
    }
  }

  goToDashboard() {
    this.router.navigate(['/']);
    this.hideSidebarOnMobile();
  }

}
