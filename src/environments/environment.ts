// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBase: "https://***.***.***.***/Services/StdClientFacingService.svc/",
  apiCollections: {
    authenticate: {
      signin:    "Auth/Login/"
    },
    order: {
      details:  "Order/GetOrderDetails/",
      progress: "Order/GetOrderProgressDetails/",
      courier: "Order/GetCourierDetails/",
      additional: "Order/GetOrderAdditionalDetails/",
      comments: "Order/GetOrderComments/",
      documents: "Order/GetDocuments/",
      dispatch: "Order/GetDispatchDetails/"
    },
    search: "Search/FindOrders"
  },
  logoUrlBase: "https://***.blob.core.windows.net/logos/",
  documentsUrlBase: "https://***.blob.core.windows.net/repository/",
  yellowfin: {
    baseUrl: "http://***.***.***.***/JsAPI",
    additionalScripts: {
      jquery: "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js",
      requireJs: "https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.6/require.min.js"
    },
    credentials: {
      username: "admin@yellowfin.com.au",
      password: "test",
    },
    reports: {
      trend: {
        orderVolume: "",
        performanceTrend: "c2f716e1-f364-4af5-8963-4d15ba0d6fa5"
      },
      contactCentre: {
        addressVerificationRate: "5d94ac14-f546-4bf5-b75b-361f17a2cf2f",
        outbound: "de26e14a-63f0-4726-be5b-3ca78bfff43b",
        inbound: "2a9e0b6c-2651-4682-8f7f-f087c3f24846"
      },
      logistics: {
        deliveryRate: "8286935f-01ca-4700-923c-ebfed810d0da",
        courierPerformance: "55129adc-ae9f-444c-b2de-d1ad86ff30dc"
      },
      nonDelivery: {
        cancellationRate: "3b0f602d-72ad-458d-8b3c-3cb6c720b21e",
        returnRate: "5a072b0e-108e-4893-bfe4-48639c5a04b1",
        cancellationReason: "542ef991-b5ea-48fb-a988-2fc3975dc3de",
        returnReason: "967dbcf7-f8ef-47b5-a70c-2b237aa37635"
      }
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
