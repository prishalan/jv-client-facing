window.yellowfin = window.yellowfin || {
   /*
    * Yellowfin External Javascript Base API
    */
   "apiVersion": "2.1",
   "baseURL": "http://13.74.251.6/JsAPI",
   "jqueryPath": "/assets/scripts/jquery.min.js",
   "requirePath": "/assets/scripts/require.min.js",

   "serverInfo": {
      "releaseVersion": "7.4",
      "buildVersion": "20180515",
      "javaVersion": "1.8.0_161",
      "operatingSystem": "Linux",
      "operatingSystemArch": "amd64",
      "operatingSystemVersion": "4.4.0-92-generic",
      "schemaVersion": "20130704"
   },

   "requests": [],
   "nextRequestId": 0,

   "traceEnabled": false,

   "trace": function(name) {
      if (this.traceEnabled) {
         this.log('TRACE: ' + name);
      }
   },
   
   "log": function(text) {
      if (window.console && console.log) {
         console.log(text);
      }
   },

   "apiError": function(object) {
      this.trace('yellowfin.yellowfin.apiError()');
      alert('API Error: ' + object.errorDescription);
   },

   "apiLoadError": function(object) {
      this.trace('yellowfin.yellowfin.apiLoadError()');
      alert('Error loading API: ' + object.errorDescription);
   },

   "insertStylesheet": function(url) {
      this.trace('yellowfin.insertStylesheet()');

      var css = document.createElement('link');
      css.type = 'text/css';
      css.rel = 'stylesheet';
      css.href = url;
      css.media = 'screen';
      var head = document.getElementsByTagName('head')[0];
      if (head.firstChild) {
         head.insertBefore(css, head.firstChild);
      } else {
         head.appendChild(css);
      }

   },

   "insertScript": function(url) {
      this.trace('yellowfin.insertScript()');

      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = url;
      document.getElementsByTagName('head')[0].appendChild(s);

   },

   "loadApi": function(type, callback, arg) {
      this.trace('yellowfin.loadApi(\'' + type + '\')');

      var url = this.baseURL + '?api=' + encodeURIComponent(type);
      if (callback && callback != '') {
         url += '&callback=' + encodeURIComponent(callback);
         if (arg != null) {
            url += '&arg=' + encodeURIComponent(arg);
         }
      }
      this.insertScript(url);
   },
   
   "loadScript": function(script, callback, arg) {
      this.trace('yellowfin.loadScript(\'' + script + '\')');
      
      var url = this.baseURL + '?load=' + script;
      if (callback && callback != '') {
         url += '&callback=' + encodeURIComponent(callback);
         if (arg) {
            url += '&arg=' + encodeURIComponent(arg);
         }
      }
      this.insertScript(url);

   },

   "getObj": function(id) {
      this.trace('yellowfin.getObj()');

      if (document.layers) return document.layers[id];
      if (document.getElementById) return document.getElementById(id);
      return document.all[id];
   },

   "hasClassName": function(element, className) {
      this.trace('yellowfin.hasClassName()');

      if (!element) return;
      var cn = element.className;
      return (cn && (cn == className || new RegExp("(^|\\s)" + className + "(\\s|$)").test(cn)));
   },

   "EventObj" : function(event, element) {

      /* the event object */
      this.event = event || window.event;

      /* mouse buttons */
      this.buttonLeft = false;
      this.buttonMiddle = false;
      this.buttonRight = false;
      if (this.event.which) {
         switch (this.event.which) {
         case 1:
            this.buttonLeft = true;
            break;
         case 2:
            this.buttonMiddle = true;
            break;
         case 3:
            this.buttonRight = true;
            break;
         }
      } else if (this.event.button) {
         this.buttonLeft = (this.event.button & 1) == 1;
         this.buttonMiddle = (this.event.button & 2) == 2;
         this.buttonRight = (this.event.button & 4) == 4;
      }

      /* mouse and window co-ordinates */
      if (this.event.pageX) {
         this.pageX = this.event.pageX;
         this.pageY = this.event.pageY;
      } else {
         this.pageX = this.event.clientX + yellowfin.getScrollLeft() - 2;
         this.pageY = this.event.clientY + yellowfin.getScrollTop() - 2;
      }
      this.clientX = this.event.clientX;
      this.clientY = this.event.clientY;

      /* source element */
      this.element = element;
      this.target = this.event.target || this.event.srcElement;
      this.fromElement = this.event.fromElement || this.event.relatedTarget;
      this.toElement = this.event.toElement || this.event.relatedTarget;

      /* prevent default function */
      this.preventDefault = function() {
         if (this.event.preventDefault) {
            this.event.preventDefault();
         } else {
            this.event.returnValue = false;
         }
      };

   },

   "loadReport": function(object) {
      this.trace('yellowfin.loadReport()');

      var rid = this.nextRequestId++;
      var robj = {
         "id": rid,
         "options": object
      };
      this.requests['r' + rid] = robj;

      if (yellowfin.reports) {
         this.loadReportNow(rid);
      } else {
         this.loadApi('reports', 'yellowfin.loadReportNow', rid);
      }
   },
   
   "loadReportNow": function(reqId) {
      this.trace('yellowfin.loadReportNow()');

      var object = this.requests['r' + reqId];
      if (!object) {
         alert('Error: invalid request');
         return;
      }
      this.requests['r' + reqId] = null;

      yellowfin.reports.loadReport(object.options);
   },

   "loadDash": function(object) {
      this.trace('yellowfin.loadDash()');

      var rid = this.nextRequestId++;
      var robj = {
         "id": rid,
         "options": object
      };
      this.requests['r' + rid] = robj;

      if (yellowfin.dash) {
         this.loadDashNow(rid);
      } else {
         this.loadApi('dash', 'yellowfin.loadDashNow', rid);
      }
   },
   
   "loadDashNow": function(reqId) {
      this.trace('yellowfin.loadDashNow()');
      
      var object = this.requests['r' + reqId];
      if (!object) {
         alert('Error: invalid request');
         return;
      }
      this.requests['r' + reqId] = null;
      
      yellowfin.dash.loadDash(object.options);
   },

   "getAbsoluteTop": function(elem) {
      this.trace('yellowfin.getAbsoluteTop()');

      var topPosition = 0;
      while (elem) {
         if (elem.tagName.toUpperCase() == 'BODY') break;
         topPosition += elem.offsetTop;
         elem = elem.offsetParent;
      }
      return topPosition;
   },

   "getAbsoluteLeft": function(elem) {
      this.trace('yellowfin.getAbsoluteLeft()');

      var leftPosition = 0;
      while (elem) {
         if (elem.tagName.toUpperCase() == 'BODY') break;
         leftPosition += elem.offsetLeft;
         elem = elem.offsetParent;
      }
      return leftPosition;
   },

   "eventOnObject": function(obj, onEvent, handler) {
      this.trace('yellowfin.eventOnObject()');

      if (obj.attachEvent) {
         obj.attachEvent("on" + onEvent, handler);
      } else {
         obj.addEventListener(onEvent, handler, true);
      }
   },

   "eventOffObject": function(obj, onEvent, handler) {
      this.trace('yellowfin.eventOffObject()');

      if (obj.detachEvent) {
         obj.detachEvent("on" + onEvent, handler);
      } else {
         obj.removeEventListener(onEvent, handler, true);
      }
   },
   
   "getScrollLeft": function() {
      this.trace('yellowfin.getScrollLeft()');

      if (document.compatMode && document.compatMode != 'BackCompat') {
         return Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);
      } else {
         return document.body.scrollLeft;
      }
   },

   "getScrollTop": function() {
      this.trace('yellowfin.getScrollTop()');

      if (document.compatMode && document.compatMode != 'BackCompat') {
         return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
      } else {
         return document.body.scrollTop;
      }
   },
   
   "escapeHtml": function(text) {
      var div = document.createElement('div');
      div.appendChild(document.createTextNode(text));
      return div.innerHTML;
   }

};
